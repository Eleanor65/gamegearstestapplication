﻿using System;
using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class InstallerSceneGame : MonoInstaller
	{
		[SerializeField] private SettingsGame _settingsGame;
		[SerializeField] private Canvas _canvas;
		[SerializeField] private ViewCamera _viewCamera;
		[SerializeField] private ViewCameraPositionZero _viewCameraPositionZero;

		public override void InstallBindings()
		{
			var providerGameData = new ProviderGameDataFromResources();
			var gameData = providerGameData.GetGameData();
			Container.Bind<IProviderGameData>().FromInstance(providerGameData).AsSingle();

			Container.BindInstance(_settingsGame);
			Container.BindInstance(_viewCamera.Camera);
			Container.Bind<IProviderCameraPositionZero>().FromInstance(_viewCameraPositionZero);

			Container.Bind<IViewCamera>().FromInstance(_viewCamera);
			Container.Bind(typeof(IInitializable), typeof(IDisposable)).To<PresenterCamera>().AsSingle().NonLazy();
			Container.Bind(typeof(ModelCamera), typeof(IInitializable), typeof(ITickable)).To<ModelCamera>().AsSingle().NonLazy();

			Container.Bind<Game>().AsSingle();

			BindPlayers();

			Container.Bind<IProviderSprites>().To<ProviderSpritesFromResources>().AsSingle();

			if (gameData.settings.allowDuplicateBuffs)
				Container.Bind<IProviderBuffsPlayer>().To<ProviderBuffsPlayerWithDuplicates>().AsSingle();
			else
				Container.Bind<IProviderBuffsPlayer>().To<ProviderBuffsPlayerNoDuplicates>().AsSingle();

			Container.Bind<IPlayerBuffApplier>().To<PlayerBuffApplierAdditevly>().AsSingle();
			Container.Bind<IDamager>().To<Damager>().AsSingle();
			Container.Bind<IDamageCalculator>().To<DamageCalculator>().AsSingle();
			Container.Bind<ILifeStealCalculator>().To<LifeStealCalculator>().AsSingle();
		}

		public void BindPlayers()
		{
			Container.Bind(typeof(IInitializable), typeof(IDisposable)).To<CreaterPresenterPlayer>().AsSingle().NonLazy();
			Container.Bind<IInitializable>().To<CreaterPlayers>().AsSingle().NonLazy();
			Container.Bind<IInitializable>().To<PlayerTargetSetter>().AsSingle().NonLazy();

			Container.BindFactory<int, int, Player, FactoryPlayer>();

			Container.BindFactory<int, IViewPlayer, FactoryViewPlayerByTeam>()
				.FromFactory<FactoryViewPlayerByTeamFromSettings>();
			Container.BindFactory<IViewPlayer, FactoryViewPlayer>().FromFactory<FactoryViewPlayerFromPrefabInSettings>();

			BindViewPresenter<UiPresenterPlayers, UiViewPlayers>(_settingsGame.PrefabUiViewPlayers);
			BindViewPresenter<UiPresenterPlayersHp, UiViewPlayersHp>(_settingsGame.PrefabUiViewPlayersHp);
			BindViewPresenter<UiPresenterPlayersHpChanges, UiViewPlayersHpChanges>(_settingsGame.PrefabUiViewPlayersHpChanges);
			BindViewPresenter<UiPresenterGameRestart, UiViewGameRestart>(_settingsGame.UiViewGameRestart);

			Container.Bind(typeof(GameStarter), typeof(IInitializable)).To<GameStarter>().AsSingle().NonLazy();
		}

		private void BindViewPresenter<TUiPresenter, TUiView>(TUiView prefabView) where TUiPresenter : BaseUiPresenter<TUiView> where TUiView : UnityEngine.Object
		{
			Container.BindInterfacesAndSelfTo<TUiPresenter>().AsSingle()
				.NonLazy();
			Container.Bind<TUiView>().FromComponentInNewPrefab(prefabView).UnderTransform(_canvas.transform).AsSingle();
		}
	}
}