﻿namespace GameGearsTestApplication
{
	public interface IProviderGameData
	{
		Data GetGameData();
	}
}