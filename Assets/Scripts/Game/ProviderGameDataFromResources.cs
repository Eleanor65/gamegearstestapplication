﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class ProviderGameDataFromResources : IProviderGameData
	{
		private const string Path = "data";

		private Data _data;

		public Data GetGameData()
		{
			if (_data != null)
				return _data;

			var asset = Resources.Load<TextAsset>(Path);
			if (asset == null)
			{
				Debug.LogError($"There is no text asset in Resources at {Path}");
				return null;
			}

			_data = JsonUtility.FromJson<Data>(asset.text);
			return _data;
		}
	}
}