﻿namespace GameGearsTestApplication
{
	public abstract class BasePresenter<TMovel, TView>
	{
		protected TMovel Model { get; }
		protected TView View { get; }

		protected BasePresenter(TMovel model, TView view)
		{
			Model = model;
			View = view;
		}
	}
}