﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameGearsTestApplication
{
	public class Game
	{
		private readonly List<Player> _players = new List<Player>();

		public IReadOnlyList<Player> Players => _players;

		public event Action<Player> OnPlayerCreated = _ => { };

		public void AddPlayer(Player player)
		{
			_players.Add(player);
			OnPlayerCreated(player);
		}

		public Player GetPlayer(int playerId)
		{
			return _players.FirstOrDefault(p => p.Id == playerId);
		}
	}
}