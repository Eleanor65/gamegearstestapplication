﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public interface IProviderCameraPositionZero
	{
		Vector3 PositionZero { get; }
	}
}