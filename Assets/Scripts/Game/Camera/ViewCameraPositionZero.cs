﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class ViewCameraPositionZero : MonoBehaviour, IProviderCameraPositionZero
	{
		public Vector3 PositionZero => transform.position;
	}
}