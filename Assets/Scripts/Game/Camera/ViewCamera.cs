﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class ViewCamera : MonoBehaviour, IViewCamera
	{
		[SerializeField] private Camera _camera;

		public Camera Camera => _camera;

		public Vector3 Position
		{
			get => transform.position;
			set => transform.position = value;
		}

		public Vector3 Forward
		{
			get => transform.forward;
			set => transform.forward = value;
		}

		public float Fov
		{
			get => _camera.fieldOfView;
			set => _camera.fieldOfView = value;
		}
	}
}