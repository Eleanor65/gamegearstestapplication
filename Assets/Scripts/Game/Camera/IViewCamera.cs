﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public interface IViewCamera
	{
		Vector3 Position { get; set; }
		Vector3 Forward { get; set; }
		float Fov { get; set; }
	}
}