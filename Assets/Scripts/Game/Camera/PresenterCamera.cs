﻿using System;
using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class PresenterCamera : BasePresenter<ModelCamera, IViewCamera>, IInitializable, IDisposable
	{
		public PresenterCamera(ModelCamera model, IViewCamera view) : base(model, view)
		{
		}

		public void Initialize()
		{
			Model.OnChangedPosition += OnModelChangedPosition;
			Model.OnChangedForward += OnModelChangedForward;
			Model.OnChangedFov += OnModelChangedFov;
		}

		public void Dispose()
		{
			Model.OnChangedPosition -= OnModelChangedPosition;
			Model.OnChangedForward -= OnModelChangedForward;
			Model.OnChangedFov -= OnModelChangedFov;
		}

		private void OnModelChangedPosition(Vector3 position)
		{
			View.Position = position;
		}

		private void OnModelChangedForward(Vector3 forward)
		{
			View.Forward = forward;
		}

		private void OnModelChangedFov(float fov)
		{
			View.Fov = fov;
		}
	}
}