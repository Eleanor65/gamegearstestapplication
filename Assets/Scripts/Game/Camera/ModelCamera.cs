﻿using System;
using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class ModelCamera : IInitializable, ITickable
	{
		private readonly CameraModel _settings;

		private readonly Vector3 _positionZero;
		private readonly Vector3 _lookPoint;
		private readonly float _radiusMin;
		private readonly float _radiusMax;

		public Vector3 Position
		{
			get => _position;
			set
			{
				if (value == _position)
					return;

				_position = value;
				OnChangedPosition(_position);
			}
		}

		public Vector3 Forward
		{
			get => _forward;
			set
			{
				if (value == _forward)
					return;

				_forward = value;
				OnChangedForward(_forward);
			}
		}

		public float Fov
		{
			get => _fov;
			set
			{
				if (Mathf.Approximately(value, _fov))
					return;

				_fov = value;
				OnChangedFov(_fov);
			}
		}

		public event Action<Vector3> OnChangedPosition = _ => { };
		public event Action<Vector3> OnChangedForward = _ => { };
		public event Action<float> OnChangedFov = _ => { };

		private float _deltaTime;
		private float _durationCircle;

		private Vector3 _position;
		private Vector3 _forward;

		private float _radius;
		private bool _isRadiusGrowing;

		private float _durationFov;
		private bool _isFovChanging;
		private float _fovSpeed;
		private float _fovTarget;
		private float _fov;

		public ModelCamera(IProviderGameData providerGameData, IProviderCameraPositionZero providerCameraPositionZero)
		{
			_settings = providerGameData.GetGameData().cameraSettings;
			_positionZero = providerCameraPositionZero.PositionZero;
			_lookPoint = _positionZero + _settings.lookAtHeight * Vector3.up;
			_radius = _settings.roundRadius;
			_radiusMin = _radius - _settings.roamingRadius;
			_radiusMax = _radius + _settings.roamingRadius;
		}

		public void Initialize()
		{
			Fov = GetFovTarget();

			SetPositionForward();
			SetFov();
		}

		public void Tick()
		{
			_deltaTime = Time.smoothDeltaTime;
			SetPositionForward();
			SetFov();
		}

		private void SetPositionForward()
		{
			_durationCircle += _deltaTime;
			if (_durationCircle >= _settings.roundDuration)
				_durationCircle -= _settings.roundDuration;

			var angle = 360f * _durationCircle / _settings.roundDuration;

			var deltaRadius = _deltaTime / _settings.roamingDuration;
			if (_isRadiusGrowing)
			{
				_radius += deltaRadius;
				if (_radius > _radiusMax)
				{
					_radius = _radiusMax;
					_isRadiusGrowing = false;
				}
			}
			else
			{
				_radius -= deltaRadius;
				if (_radius < _radiusMin)
				{
					_radius = _radiusMin;
					_isRadiusGrowing = true;
				}
			}

			var position = Quaternion.AngleAxis(angle, Vector3.up) * Vector3.right * _radius;
			position += _positionZero;
			position += _settings.height * Vector3.up;
			Position = position;

			Forward = _lookPoint - position;
		}

		private void SetFov()
		{
			_durationFov += _deltaTime;

			if (_isFovChanging)
			{
				if (_durationFov >= _settings.fovDuration)
				{
					_durationFov = 0f;
					_isFovChanging = false;
					Fov = _fovTarget;
					return;
				}

				Fov += _fovSpeed * _deltaTime;
				return;
			}

			if ((_durationFov < _settings.fovDelay))
				return;

			_durationFov = 0f;
			_isFovChanging = true;
			_fovTarget = GetFovTarget();
			_fovSpeed = (_fovTarget - Fov) / _settings.fovDuration;
		}

		private float GetFovTarget()
		{
			return UnityEngine.Random.Range(_settings.fovMin, _settings.fovMax);
		}
	}
}