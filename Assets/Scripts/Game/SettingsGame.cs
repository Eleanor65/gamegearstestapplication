﻿using UnityEngine;

namespace GameGearsTestApplication
{
	[CreateAssetMenu]
	public class SettingsGame : ScriptableObject
	{
		[SerializeField] private ViewPlayer _prefabPlayer;
		[SerializeField] private Material[] _materialsPlayer;
		[Header("Ui")]
		[SerializeField] private UiViewPlayers _prefabUiViewPlayers;
		[SerializeField] private UiViewPlayersHp _prefabUiViewPlayersHp;
		[SerializeField] private UiViewPlayersHpChanges _prefabUiViewPlayersHpChanges;
		[SerializeField] private UiViewGameRestart _uiViewGameRestart;

		public ViewPlayer PrefabPlayer => _prefabPlayer;
		public Material[] MaterialsPlayer => _materialsPlayer;
		public UiViewPlayers PrefabUiViewPlayers => _prefabUiViewPlayers;
		public UiViewPlayersHp PrefabUiViewPlayersHp => _prefabUiViewPlayersHp;
		public UiViewPlayersHpChanges PrefabUiViewPlayersHpChanges => _prefabUiViewPlayersHpChanges;
		public UiViewGameRestart UiViewGameRestart => _uiViewGameRestart;
	}
}