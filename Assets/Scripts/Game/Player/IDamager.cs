﻿namespace GameGearsTestApplication
{
	public interface IDamager
	{
		void Damage(Player attacker, Player receiver);
	}
}