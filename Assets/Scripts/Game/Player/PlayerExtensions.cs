﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public static class PlayerExtensions
	{
		public static float GetStatValue(this Player player, int id)
		{
			switch (id)
			{
				case StatConsts.HealthPoints:
					return player.Hp;

				case StatConsts.Armor:
					return player.Armor;

				case StatConsts.Damage:
					return player.Damage;

				case StatConsts.LifeSteal:
					return player.LifeSteal;
			}

			Debug.LogError($"There is no stat with id {id}");
			return -1f;
		}


		public static void SetStatValue(this Player player, int id, float value)
		{
			switch (id)
			{
				case StatConsts.HealthPoints:
					player.MaxHp = value;
					player.Hp = value;
					return;

				case StatConsts.Armor:
					player.Armor = value;
					return;

				case StatConsts.Damage:
					player.Damage = value;
					return;

				case StatConsts.LifeSteal:
					player.LifeSteal = value;
					return;
			}

			Debug.LogError($"There is no stat with id {id}");
		}
	}
}