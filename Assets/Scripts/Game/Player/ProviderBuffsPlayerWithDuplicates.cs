﻿using System.Collections.Generic;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class ProviderBuffsPlayerWithDuplicates : IProviderBuffsPlayer
	{
		private readonly IProviderGameData _providerGameData;

		public ProviderBuffsPlayerWithDuplicates(IProviderGameData providerGameData)
		{
			_providerGameData = providerGameData;
		}

		public List<Buff> GetBuffsForPlayer()
		{
			var data = _providerGameData.GetGameData();

			if (data.settings.buffCountMin > data.settings.buffCountMax)
			{
				Debug.LogError(
					$"BuffCountMin {data.settings.buffCountMin} must not be greater than BuffCountMax {data.settings.buffCountMax}");
				return null;
			}

			var buffCount = Random.Range(data.settings.buffCountMin, data.settings.buffCountMax);
			var result = new List<Buff>();

			for (var i = 0; i < buffCount; i++)
			{
				var randomIndex = Random.Range(0, data.buffs.Length);
				result.Add(data.buffs[randomIndex]);
			}

			return result;
		}
	}
}