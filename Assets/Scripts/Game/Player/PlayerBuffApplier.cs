﻿namespace GameGearsTestApplication
{
	public class PlayerBuffApplier : IPlayerBuffApplier
	{
		public void ApplyBuffs(Player player)
		{
			if (player.Buffs == null || player.Buffs.Count == 0)
				return;

			foreach (var buff in player.Buffs)
			{
				foreach (var buffStat in buff.stats)
				{
					player.SetStatValue(buffStat.statId, buffStat.value);
				}
			}
		}
	}
}