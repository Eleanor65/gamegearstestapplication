﻿namespace GameGearsTestApplication
{
	public interface IPlayerBuffApplier
	{
		void ApplyBuffs(Player player);
	}
}