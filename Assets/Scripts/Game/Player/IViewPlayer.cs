﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public interface IViewPlayer
	{
		Vector3 Postion { get; set; }
		Vector3 Forward { get; set; }

		int Hp { set; }
		
		void SetMaterial(Material material);
		void Attack();
	}
}