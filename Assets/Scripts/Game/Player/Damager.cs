﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class Damager : IDamager
	{
		private readonly IDamageCalculator _damageCalculator;
		private readonly ILifeStealCalculator _lifeStealCalculator;

		public Damager(IDamageCalculator damageCalculator, ILifeStealCalculator lifeStealCalculator)
		{
			_damageCalculator = damageCalculator;
			_lifeStealCalculator = lifeStealCalculator;
		}

		public void Damage(Player attacker, Player receiver)
		{
			if (attacker == null)
			{
				Debug.LogError("Attacker cannot be null!");
				return;
			}

			if (receiver == null || !receiver.IsAlive)
				return;
			
			var damage = _damageCalculator.GetDamage(attacker, receiver);
			receiver.Hp -= damage;

			var lifeSteal = _lifeStealCalculator.GetLifeSteal(attacker, damage);
			attacker.Hp = Mathf.Clamp(attacker.Hp + lifeSteal, 0f, attacker.MaxHp);
		}
	}
}