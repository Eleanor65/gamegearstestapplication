﻿using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class CreaterPlayers : IInitializable
	{
		private readonly IProviderGameData _providerGameData;
		private readonly Game _game;
		private readonly FactoryPlayer _factoryPlayer;

		private int _playerId;

		public CreaterPlayers(IProviderGameData providerGameData,
			Game game,
			FactoryPlayer factoryPlayer)
		{
			_providerGameData = providerGameData;
			_game = game;
			_factoryPlayer = factoryPlayer;
		}

		public void Initialize()
		{
			var data = _providerGameData.GetGameData();
			var playersCount = data.settings.playersCount;
			if (playersCount <= 0)
			{
				Debug.LogError($"Can't create game with {playersCount} players");
				return;
			}

			for (var i = 0; i < playersCount; i++)
			{
				var team = _playerId % 2;
				var player = _factoryPlayer.Create(_playerId, team);
				_playerId++;

				var pX = player.Team == 0 ? 1 : -1;
				var pZ = 2 * (player.Id / 2);
				var position = new Vector3(pX, 0f, pZ);
				player.Position = position;

				var forward = pX > 0 ? Vector3.left : Vector3.right;
				player.Forward = forward;

				_game.AddPlayer(player);
			}
		}
	}
}