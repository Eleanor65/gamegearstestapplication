﻿using System.Collections.Generic;

namespace GameGearsTestApplication
{
	public interface IProviderBuffsPlayer
	{
		List<Buff> GetBuffsForPlayer();
	}
}