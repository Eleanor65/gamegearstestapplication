﻿namespace GameGearsTestApplication
{
	public interface ILifeStealCalculator
	{
		float GetLifeSteal(Player player, float damage);
	}
}