﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class DamageCalculator : IDamageCalculator
	{
		public float GetDamage(Player attacker, Player receiver)
		{
			if (attacker == null || receiver == null)
			{
				Debug.LogError("attacker and receiver must not be null!");
				return 0f;
			}

			var damage = attacker.Damage * (1f - receiver.Armor / 100f);
			damage = Mathf.Clamp(damage, 0f, receiver.Hp);
			return damage;
		}
	}
}