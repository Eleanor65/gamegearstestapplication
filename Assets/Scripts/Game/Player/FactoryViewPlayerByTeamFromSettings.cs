﻿using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class FactoryViewPlayerByTeamFromSettings : IFactory<int, IViewPlayer>
	{
		private readonly SettingsGame _settingsGame;
		private readonly FactoryViewPlayer _factoryViewPlayer;

		public FactoryViewPlayerByTeamFromSettings(SettingsGame settingsGame, FactoryViewPlayer factoryViewPlayer)
		{
			_settingsGame = settingsGame;
			_factoryViewPlayer = factoryViewPlayer;
		}

		public IViewPlayer Create(int team)
		{
			var view = _factoryViewPlayer.Create();

			if (_settingsGame.MaterialsPlayer.Length == 0)
			{
				Debug.LogError("There are no materials in settings");
				return view;
			}

			if (team < 0 || team >= _settingsGame.MaterialsPlayer.Length)
			{
				Debug.LogError($"There is no material for team {team}");
				return view;
			}

			var material = _settingsGame.MaterialsPlayer[team];
			view.SetMaterial(material);

			return view;
		}
	}
}