﻿using System;
using System.Collections.Generic;
using Zenject;

namespace GameGearsTestApplication
{
	public class CreaterPresenterPlayer : IInitializable, IDisposable
	{
		private readonly Game _game;
		private readonly FactoryViewPlayerByTeam _factoryViewPlayer;
		private readonly List<PresenterPlayer> _presenters = new List<PresenterPlayer>();

		public CreaterPresenterPlayer(Game game, FactoryViewPlayerByTeam factoryViewPlayer)
		{
			_game = game;
			_factoryViewPlayer = factoryViewPlayer;
		}

		public void Initialize()
		{
			_game.OnPlayerCreated += OnPlayerCreated;
		}

		public void Dispose()
		{
			_game.OnPlayerCreated -= OnPlayerCreated;

			foreach (var presenterPlayer in _presenters)
			{
				presenterPlayer.Dispose();
			}
			_presenters.Clear();
		}

		private void OnPlayerCreated(Player player)
		{
			var view = _factoryViewPlayer.Create(player.Team);
			var presenter = new PresenterPlayer(player, view);
			_presenters.Add(presenter);
		}
	}
}