﻿using System;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class PresenterPlayer : BasePresenter<Player, IViewPlayer>, IDisposable
	{
		public PresenterPlayer(Player model, IViewPlayer view) : base(model, view)
		{
			View.Postion = Model.Position;
			View.Forward = Model.Forward;

			Model.OnHpChanged += OnModelChangedHp;
			OnModelChangedHp(Model);

			Model.OnAttack += OnModelAttack;
		}

		public void Dispose()
		{
			Model.OnHpChanged -= OnModelChangedHp;
			Model.OnAttack -= OnModelAttack;
		}

		private void OnModelChangedHp(Player player)
		{
			View.Hp = Mathf.CeilToInt(player.Hp);
		}

		private void OnModelAttack()
		{
			View.Attack();
		}
	}
}