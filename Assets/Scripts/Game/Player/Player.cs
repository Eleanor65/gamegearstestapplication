﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class Player
	{
		private readonly IPlayerBuffApplier _playerBuffApplier;
		private readonly IDamager _damager;

		public int Id { get; }
		public int Team { get; }

		public Vector3 Position { get; set; }
		public Vector3 Forward { get; set; }

		public Stat[] Stats { get; private set; }
		public List<Buff> Buffs { get; private set; }

		public Player Target { get; set; }

		public float MaxHp { get; set; }
		public float Hp
		{
			get => _health;
			set
			{
				if (Mathf.Approximately(value, _health))
					return;

				if (_isInited)
				{
					HpDelta = value - _health;
					_health = value;
					OnHpChanged(this);
					return;
				}

				HpDelta += value - _health;
				_health = value;
			}
		}

		public float HpDelta { get; private set; }

		public float Damage { get; set; }
		public float Armor { get; set; }
		public float LifeSteal { get; set; }

		public bool IsAlive => Hp > 0;

		public event Action<Player> OnInited = _ => { };
		public event Action<Player> OnHpChanged = _ => { };
		public event Action OnAttack = () => { };
		
		private float _health;
		private bool _isInited;

		public Player(int id,
			int team,
			IPlayerBuffApplier playerBuffApplier,
			IDamager damager)
		{
			_playerBuffApplier = playerBuffApplier;
			_damager = damager;
			Id = id;
			Team = team;
		}

		public void Init(Stat[] stats, List<Buff> buffs)
		{
			_isInited = false;
			HpDelta = 0f;

			SetStats(stats);
			SetBuffs(buffs);

			_isInited = true;
			OnInited(this);

			if (!Mathf.Approximately(HpDelta, 0f))
				OnHpChanged(this);
		}

		public void Attack()
		{
			if (!IsAlive)
				return;

			if (Target == null || !Target.IsAlive)
				return;

			OnAttack();

			_damager.Damage(this, Target);
		}

		private void SetStats(Stat[] stats)
		{
			Stats = stats;

			var stat = GetStat(StatConsts.HealthPoints);
			if (stat == null)
			{
				Debug.LogError($"There is no hp stat for player {Id}");
				MaxHp = Hp;
				Hp = 0f;
			}
			else
			{
				MaxHp = stat.value;
				Hp = stat.value;
			}

			stat = GetStat(StatConsts.Damage);
			if (stat == null)
			{
				Debug.LogError($"There is no damage stat for player {Id}");
				Damage = 0f;
			}
			else
			{
				Damage = stat.value;
			}

			stat = GetStat(StatConsts.Armor);
			if (stat == null)
			{
				Debug.LogError($"There is no armor stat for player {Id}");
				Armor = 0f;
			}
			else
			{
				Armor = stat.value;
			}

			stat = GetStat(StatConsts.LifeSteal);
			if (stat == null)
			{
				Debug.LogError($"There is no vampirism stat for player {Id}");
				LifeSteal = 0f;
			}
			else
			{
				LifeSteal = stat.value;
			}
		}

		private void SetBuffs(List<Buff> buffs)
		{
			Buffs = buffs;
			_playerBuffApplier.ApplyBuffs(this);
		}

		private Stat GetStat(int id)
		{
			return Stats.FirstOrDefault(s => s.id == id);
		}
	}
}