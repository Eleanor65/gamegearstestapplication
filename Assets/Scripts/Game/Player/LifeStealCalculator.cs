﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class LifeStealCalculator : ILifeStealCalculator
	{
		public float GetLifeSteal(Player player, float damage)
		{
			if (player == null)
			{
				Debug.LogError("Player must not be null");
				return 0f;
			}

			return damage * player.LifeSteal / 100f;
		}
	}
}