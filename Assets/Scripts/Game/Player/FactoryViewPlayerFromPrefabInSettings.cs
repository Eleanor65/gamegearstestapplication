﻿using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class FactoryViewPlayerFromPrefabInSettings : IFactory<IViewPlayer>
	{
		private readonly SettingsGame _settingsGame;

		private Transform Root => _root ?? (_root = new GameObject("Players").transform);

		private Transform _root;

		public FactoryViewPlayerFromPrefabInSettings(SettingsGame settingsGame)
		{
			_settingsGame = settingsGame;
		}

		public IViewPlayer Create()
		{
			return Object.Instantiate(_settingsGame.PrefabPlayer, Root);
		}
	}
}