﻿using Zenject;

namespace GameGearsTestApplication
{
	public class PlayerTargetSetter : IInitializable
	{
		private readonly Game _game;

		public PlayerTargetSetter(Game game)
		{
			_game = game;
		}

		public void Initialize()
		{
			for (var i = 1; i < _game.Players.Count; i += 2)
			{
				_game.Players[i].Target = _game.Players[i - 1];
				_game.Players[i - 1].Target = _game.Players[i];
			}
		}
	}
}