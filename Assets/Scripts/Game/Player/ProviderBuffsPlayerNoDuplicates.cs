﻿using System.Collections.Generic;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class ProviderBuffsPlayerNoDuplicates : IProviderBuffsPlayer
	{
		private readonly IProviderGameData _providerGameData;
		private readonly List<Buff> _buffer = new List<Buff>();

		public ProviderBuffsPlayerNoDuplicates(IProviderGameData providerGameData)
		{
			_providerGameData = providerGameData;
		}

		public List<Buff> GetBuffsForPlayer()
		{
			var data = _providerGameData.GetGameData();

			if (data.settings.buffCountMin > data.settings.buffCountMax)
			{
				Debug.LogError(
					$"BuffCountMin {data.settings.buffCountMin} must not be greater than BuffCountMax {data.settings.buffCountMax}");
				return null;
			}

			var buffCount = Random.Range(data.settings.buffCountMin, data.settings.buffCountMax);
			var result = new List<Buff>();

			foreach (var dataBuff in data.buffs)
			{
				_buffer.Add(dataBuff);
			}

			for (var i = 0; i < buffCount && _buffer.Count > 0; i++)
			{
				var randomIndex = Random.Range(0, _buffer.Count);
				result.Add(_buffer[randomIndex]);
				_buffer.RemoveAt(randomIndex);
			}

			_buffer.Clear();

			return result;
		}
	}
}