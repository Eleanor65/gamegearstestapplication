﻿namespace GameGearsTestApplication
{
	public static class StatConsts
	{
		public const int HealthPoints = 0;
		public const int Armor = 1;
		public const int Damage = 2;
		public const int LifeSteal = 3;
	}
}