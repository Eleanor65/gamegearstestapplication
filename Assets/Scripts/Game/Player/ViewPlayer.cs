﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class ViewPlayer : MonoBehaviour, IViewPlayer
	{
		[SerializeField] private Renderer _renderer;
		[SerializeField] private Animator _animator;

		private static readonly int _parameterHealth = Animator.StringToHash("Health");
		private static readonly int _parameterAttack = Animator.StringToHash("Attack");

		public Vector3 Postion
		{
			get => transform.position;
			set => transform.position = value;
		}

		public Vector3 Forward
		{
			get => transform.forward;
			set => transform.forward = value;
		}

		public int Hp
		{
			set => _animator.SetInteger(_parameterHealth, value);
		}

		public void SetMaterial(Material material)
		{
			_renderer.material = material;
		}

		public void Attack()
		{
			_animator.SetTrigger(_parameterAttack);
		}
	}
}