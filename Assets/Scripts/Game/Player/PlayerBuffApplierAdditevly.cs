﻿namespace GameGearsTestApplication
{
	public class PlayerBuffApplierAdditevly : IPlayerBuffApplier
	{
		public void ApplyBuffs(Player player)
		{
			if (player.Buffs == null || player.Buffs.Count == 0)
				return;

			foreach (var buff in player.Buffs)
			{
				foreach (var buffStat in buff.stats)
				{
					var value = player.GetStatValue(buffStat.statId) + buffStat.value;
					player.SetStatValue(buffStat.statId, value);
				}
			}
		}
	}
}