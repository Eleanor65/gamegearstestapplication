﻿namespace GameGearsTestApplication
{
	public interface IDamageCalculator
	{
		float GetDamage(Player attacker, Player receiver);
	}
}