﻿using System.Collections.Generic;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class ProviderSpritesFromResources : IProviderSprites
	{
		private const string Folder = "Icons/";

		private readonly Dictionary<string, Sprite> _sprites = new Dictionary<string, Sprite>();

		public Sprite GetSprite(string name)
		{
			if (_sprites.TryGetValue(name, out var sprite))
			{
				return sprite;
			}

			sprite = Resources.Load<Sprite>(Folder + name);
			_sprites.Add(name, sprite);
			return sprite;
		}
	}
}