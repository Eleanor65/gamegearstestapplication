﻿namespace GameGearsTestApplication
{
	public class UiPresenterGameRestart : BaseUiPresenter<UiViewGameRestart>
	{
		private readonly GameStarter _gameStarter;

		public UiPresenterGameRestart(UiViewGameRestart view, GameStarter gameStarter) : base(view)
		{
			_gameStarter = gameStarter;
		}

		public override void Initialize()
		{
			base.Initialize();

			View.OnClickRestart += OnClickRestart;
		}

		public override void Dispose()
		{
			base.Dispose();

			View.OnClickRestart -= OnClickRestart;
		}

		private void OnClickRestart(bool withBuffs)
		{
			if (withBuffs)
				_gameStarter.StartWithBuffs();
			else
				_gameStarter.StartWithoutBuffs();
		}
	}
}