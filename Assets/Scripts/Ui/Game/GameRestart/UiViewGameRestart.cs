﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameGearsTestApplication
{
	public class UiViewGameRestart : MonoBehaviour
	{
		[SerializeField] private Button _buttonRestartWithBuffs;
		[SerializeField] private Button _buttonRestartNoBuffs;

		public event Action<bool> OnClickRestart = _ => { };

		private void Awake()
		{
			_buttonRestartWithBuffs.onClick.AddListener(() => OnClickRestart(true));
			_buttonRestartNoBuffs.onClick.AddListener(() => OnClickRestart(false));
		}
	}
}