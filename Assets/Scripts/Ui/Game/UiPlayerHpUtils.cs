﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public static class UiPlayerHpUtils
	{
		public static readonly Vector3 HpOffset = Vector3.up * 3.5f;
	}
}