﻿using Zenject;

namespace GameGearsTestApplication
{
	public class GameStarter : IInitializable
	{
		private readonly Game _game;
		private readonly IProviderGameData _providerGameData;
		private readonly IProviderBuffsPlayer _providerBuffs;

		public GameStarter(Game game,
			IProviderGameData providerGameData,
			IProviderBuffsPlayer providerBuffs)
		{
			_game = game;
			_providerGameData = providerGameData;
			_providerBuffs = providerBuffs;
		}

		public void Initialize()
		{
			StartWithBuffs();
		}

		public void StartWithBuffs()
		{
			var data = _providerGameData.GetGameData();
			foreach (var player in _game.Players)
			{
				player.Init(data.stats, _providerBuffs.GetBuffsForPlayer());
			}
		}

		public void StartWithoutBuffs()
		{
			var data = _providerGameData.GetGameData();
			foreach (var player in _game.Players)
			{
				player.Init(data.stats, null);
			}
		}
	}
}