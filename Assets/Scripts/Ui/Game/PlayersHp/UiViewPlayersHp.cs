﻿using System.Collections.Generic;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class UiViewPlayersHp : MonoBehaviour
	{
		[SerializeField] private UiViewPlayerHp _prefabPlayerHp;
		[SerializeField] private RectTransform _root;

		private readonly Dictionary<int, UiViewPlayerHp> _viewsHp = new Dictionary<int, UiViewPlayerHp>();

		public void SetPlayerPosition(int playerId, Vector2 position)
		{
			var viewHp = GetViewHp(playerId);
			viewHp.RectTransform.anchoredPosition = position;
		}

		public void SetPlayerHp(int playerId, float hp, float hpMax)
		{
			var viewHp = GetViewHp(playerId);
			viewHp.SetHp(hp, hpMax);
		}

		private UiViewPlayerHp GetViewHp(int playerId)
		{
			if (_viewsHp.TryGetValue(playerId, out var viewHp))
				return viewHp;

			viewHp = Instantiate(_prefabPlayerHp, _root);
			_viewsHp.Add(playerId, viewHp);
			return viewHp;
		}
	}
}