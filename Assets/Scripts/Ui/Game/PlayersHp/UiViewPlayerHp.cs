﻿using UnityEngine;
using UnityEngine.UI;

namespace GameGearsTestApplication
{
	public class UiViewPlayerHp : MonoBehaviour
	{
		[SerializeField] private Image _image;

		public RectTransform RectTransform => _rectTransform ?? (_rectTransform = transform as RectTransform);

		private RectTransform _rectTransform;

		public void SetHp(float hp, float hpMax)
		{
			_image.fillAmount = hp / hpMax;
		}
	}
}