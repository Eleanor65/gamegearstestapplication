﻿using UnityEngine;
using Zenject;

namespace GameGearsTestApplication
{
	public class UiPresenterPlayersHp : BaseUiPresenter<UiViewPlayersHp>, ITickable
	{
		private readonly Game _game;
		private readonly Camera _camera;

		public UiPresenterPlayersHp(UiViewPlayersHp view,
			Game game,
			Camera camera)
			: base(view)
		{
			_game = game;
			_camera = camera;
		}

		public override void Initialize()
		{
			base.Initialize();

			foreach (var player in _game.Players)
			{
				player.OnHpChanged += OnPlayerChangedHp;
				OnPlayerChangedHp(player);
			}
		}

		public override void Dispose()
		{
			base.Dispose();

			foreach (var player in _game.Players)
			{
				player.OnHpChanged -= OnPlayerChangedHp;
			}
		}

		public void Tick()
		{
			foreach (var player in _game.Players)
			{
				View.SetPlayerPosition(player.Id, GetHpPosition(player));
			}
		}

		private void OnPlayerChangedHp(Player player)
		{
			View.SetPlayerHp(player.Id, player.Hp, player.MaxHp);
		}

		private Vector2 GetHpPosition(Player player)
		{
			var position = player.Position + UiPlayerHpUtils.HpOffset;
			return _camera.WorldToScreenPoint(position);
		}
	}
}