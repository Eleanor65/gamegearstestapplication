﻿using System;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class UiViewPlayers : MonoBehaviour
	{
		[SerializeField] private UiViewPlayer[] _players;

		public event Action<int> OnPlayerClickedAttack = _ => { };

		private void Awake()
		{
			for (var i = 0; i < _players.Length; i++)
			{
				var playerId = i;
				_players[i].OnClickedAttack += () => OnPlayerClickedAttack(playerId);
			}
		}

		public void SetPlayerStat(int playerId, int statId, float value, Sprite sprite)
		{
			var viewPlayer = GetViewPlayer(playerId);
			if (viewPlayer == null)
				return;

			viewPlayer.SetStat(statId, value.ToString(), sprite);
		}

		/// <summary>
		/// Sets player stat with value, but doesn't set sprite. Use to update existing value
		/// </summary>
		/// <param name="playerId"></param>
		/// <param name="statId"></param>
		/// <param name="value"></param>
		public void SetPlayerStat(int playerId, int statId, float value)
		{
			var viewPlayer = GetViewPlayer(playerId);
			if (viewPlayer == null)
				return;

			viewPlayer.SetStat(statId, value.ToString());
		}

		public void RemoveBuffs(int playerId)
		{
			var viewPlayer = GetViewPlayer(playerId);
			if (viewPlayer == null)
				return;

			viewPlayer.RemoveBuffs();
		}

		public void SetPlayerBuff(int playerId, string text, Sprite sprite)
		{
			var viewPlayer = GetViewPlayer(playerId);
			if (viewPlayer == null)
				return;

			viewPlayer.SetBuff(text, sprite);
		}

		private UiViewPlayer GetViewPlayer(int playerId)
		{
			if (playerId < 0 || playerId >= _players.Length)
			{
				Debug.LogError($"Can't get UiViewPlayer with index {playerId}");
				return null;
			}

			var viewPlayer = _players[playerId];
			if (viewPlayer == null)
			{
				Debug.LogError($"There is no UiViewPlayer at index {playerId}");
				return null;
			}

			return viewPlayer;
		}
	}
}