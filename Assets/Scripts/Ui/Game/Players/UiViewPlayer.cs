﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameGearsTestApplication
{
	public class UiViewPlayer : MonoBehaviour
	{
		[SerializeField] private Button _buttonAttack;
		[SerializeField] private UiViewPlayerStat _prefabStat;
		[SerializeField] private RectTransform _rootStats;

		private readonly Dictionary<int, UiViewPlayerStat> _viewsStat = new Dictionary<int, UiViewPlayerStat>();
		private readonly List<UiViewPlayerStat> _viewsBuffs = new List<UiViewPlayerStat>();
		private readonly List<UiViewPlayerStat> _viewsInactive = new List<UiViewPlayerStat>();

		public event Action OnClickedAttack = () => { };
		
		private void Awake()
		{
			_buttonAttack.onClick.AddListener(() => OnClickedAttack());
		}

		public void SetStat(int id, string text, Sprite sprite)
		{
			var viewStat = GetViewStat(id);
			viewStat.SetData(sprite, text);
		}

		public void SetStat(int id, string text)
		{
			var viewStat = GetViewStat(id);
			viewStat.SetText(text);
		}

		public void RemoveBuffs()
		{
			for(var i = _viewsBuffs.Count-1; i >= 0; i--)
			{
				var view = _viewsBuffs[i];
				view.gameObject.SetActive(false);
				_viewsInactive.Add(view);
				_viewsBuffs.RemoveAt(i);
			}
		}

		public void SetBuff(string text, Sprite sprite)
		{
			var viewStat = SpawnViewStat();
			_viewsBuffs.Add(viewStat);

			viewStat.SetData(sprite, text);
		}

		private UiViewPlayerStat GetViewStat(int id)
		{
			if (_viewsStat.TryGetValue(id, out var viewStat))
				return viewStat;

			viewStat = SpawnViewStat();
			_viewsStat.Add(id, viewStat);
			return viewStat;
		}

		private UiViewPlayerStat SpawnViewStat()
		{
			if (_viewsInactive.Count == 0)
				return Instantiate(_prefabStat, _rootStats);

			//should just use regular pool.
			var index = _viewsInactive.Count - 1;
			var view = _viewsInactive[index];
			view.gameObject.SetActive(true);
			_viewsInactive.RemoveAt(index);
			return view;
		}
	}
}