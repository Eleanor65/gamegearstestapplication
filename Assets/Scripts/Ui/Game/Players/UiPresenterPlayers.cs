﻿namespace GameGearsTestApplication
{
	public class UiPresenterPlayers : BaseUiPresenter<UiViewPlayers>
	{
		//mb store this in view?
		private const int MaxPlayers = 2;

		private readonly Game _game;
		private readonly IProviderSprites _providerSprites;

		public UiPresenterPlayers(UiViewPlayers view,
			Game game,
			IProviderSprites providerSprites)
			: base(view)
		{
			_game = game;
			_providerSprites = providerSprites;
		}

		public override void Initialize()
		{
			base.Initialize();

			View.OnPlayerClickedAttack += OnViewPlayerAttack;

			for (var i = 0; i < MaxPlayers && i < _game.Players.Count; i++)
			{
				var player = _game.Players[i];
				player.OnInited += OnPlayerInited;

				player.OnHpChanged += OnPlayerChangedHp;
			}
		}

		public override void Dispose()
		{
			base.Dispose();

			View.OnPlayerClickedAttack -= OnViewPlayerAttack;

			for (var i = 0; i < MaxPlayers && i < _game.Players.Count; i++)
			{
				var player = _game.Players[i];
				player.OnInited -= OnPlayerInited;

				player.OnHpChanged -= OnPlayerChangedHp;
			}
		}

		private void OnViewPlayerAttack(int playerId)
		{
			_game.GetPlayer(playerId)?.Attack();
		}

		private void OnPlayerChangedHp(Player player)
		{
			View.SetPlayerStat(player.Id, StatConsts.HealthPoints, player.Hp);
		}

		private void OnPlayerInited(Player player)
		{
			foreach (var playerStat in player.Stats)
			{
				var sprite = _providerSprites.GetSprite(playerStat.icon);
				View.SetPlayerStat(player.Id, playerStat.id, player.GetStatValue(playerStat.id), sprite);
			}

			View.RemoveBuffs(player.Id);

			if (player.Buffs == null)
				return;

			foreach (var playerBuff in player.Buffs)
			{
				var sprite = _providerSprites.GetSprite(playerBuff.icon);
				View.SetPlayerBuff(player.Id, playerBuff.title, sprite);
			}
		}
	}
}