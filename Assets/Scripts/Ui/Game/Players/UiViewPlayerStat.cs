﻿using UnityEngine;
using UnityEngine.UI;

namespace GameGearsTestApplication
{
	public class UiViewPlayerStat : MonoBehaviour
	{
		[SerializeField] private Image _image;
		[SerializeField] private Text _text;

		public void SetText(string text)
		{
			_text.text = text;
		}

		public void SetData(Sprite sprite, string text)
		{
			_image.sprite = sprite;
			_text.text = text;
		}
	}
}