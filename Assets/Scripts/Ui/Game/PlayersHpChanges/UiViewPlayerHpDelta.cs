﻿using UnityEngine;
using UnityEngine.UI;

namespace GameGearsTestApplication
{
	public class UiViewPlayerHpDelta : MonoBehaviour
	{
		[SerializeField] private Text _text;

		public RectTransform RectTransfrom => _rectTransform ?? (_rectTransform = transform as RectTransform);
		public float LifeDuration { get; set; }

		private RectTransform _rectTransform;

		public void SetText(string text, Color color)
		{
			_text.text = text;
			_text.color = color;
		}
	}
}