﻿using System.Collections.Generic;
using UnityEngine;

namespace GameGearsTestApplication
{
	public class UiViewPlayersHpChanges : MonoBehaviour
	{
		private const float Duration = 1f;

		private readonly Vector2 _speed = Vector2.up * 100f;

		private readonly List<UiViewPlayerHpDelta> _viewsActive = new List<UiViewPlayerHpDelta>();
		private readonly List<UiViewPlayerHpDelta> _viewsInactive = new List<UiViewPlayerHpDelta>();

		[SerializeField] private UiViewPlayerHpDelta _prefabHp;
		[SerializeField] private RectTransform _root;
		[SerializeField] private Color _colorPositive;
		[SerializeField] private Color _colorNegative;

		private void Update()
		{
			for (var i = _viewsActive.Count - 1; i >= 0; i--)
			{
				var view = _viewsActive[i];
				view.LifeDuration += Time.deltaTime;

				if (view.LifeDuration >= Duration)
				{
					_viewsActive.RemoveAt(i);
					view.gameObject.SetActive(false);
					_viewsInactive.Add(view);
					continue;
				}

				view.RectTransfrom.anchoredPosition += _speed * Time.smoothDeltaTime;
			}
		}

		public void SetHpDelta(float delta, Vector2 position)
		{
			var view = SpawnViewHp();
			view.RectTransfrom.anchoredPosition = position;
			var color = delta > 0 ? _colorPositive : _colorNegative;
			var text = Mathf.Round(delta).ToString("+#;-#");
			view.SetText(text, color);
			view.LifeDuration = 0f;
			_viewsActive.Add(view);
		}

		private UiViewPlayerHpDelta SpawnViewHp()
		{
			if (_viewsInactive.Count > 0)
			{
				var view = _viewsInactive[0];
				_viewsInactive.RemoveAt(0);
				view.gameObject.SetActive(true);
				return view;
			}

			return Instantiate(_prefabHp, _root);
		}
	}
}