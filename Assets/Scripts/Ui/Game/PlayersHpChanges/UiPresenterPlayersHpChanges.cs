﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public class UiPresenterPlayersHpChanges : BaseUiPresenter<UiViewPlayersHpChanges>
	{
		private readonly Game _game;
		private readonly Camera _camera;

		public UiPresenterPlayersHpChanges(UiViewPlayersHpChanges view,
			Game game,
			Camera camera)
			: base(view)
		{
			_game = game;
			_camera = camera;
		}

		public override void Initialize()
		{
			base.Initialize();

			foreach (var player in _game.Players)
			{
				player.OnHpChanged += OnPlayerHpChanged;
			}
		}

		public override void Dispose()
		{
			base.Dispose();

			foreach (var player in _game.Players)
			{
				player.OnHpChanged -= OnPlayerHpChanged;
			}
		}

		private void OnPlayerHpChanged(Player player)
		{
			var position = player.Position + UiPlayerHpUtils.HpOffset;
			var hpPosition = _camera.WorldToScreenPoint(position);
			View.SetHpDelta(player.HpDelta, hpPosition);
		}
	}
}