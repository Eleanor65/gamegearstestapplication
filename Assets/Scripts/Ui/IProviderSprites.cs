﻿using UnityEngine;

namespace GameGearsTestApplication
{
	public interface IProviderSprites
	{
		Sprite GetSprite(string name);
	}
}