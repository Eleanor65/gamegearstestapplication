﻿using System;
using Zenject;

namespace GameGearsTestApplication
{
	public abstract class BaseUiPresenter<TView>: IInitializable, IDisposable
	{
		protected TView View { get; }

		protected BaseUiPresenter(TView view)
		{
			View = view;
		}

		public virtual void Initialize()
		{
		}

		public virtual void Dispose()
		{
		}
	}
}